##builder
FROM maven:3.8.1-openjdk-15 AS builder
LABEL maintainer="mail@ngeorg.de"

RUN mkdir -p /app
WORKDIR /app
COPY . ./

RUN rm -rf ./target
RUN mvn install
RUN chmod -R 777 /app/target
RUN ls -ll /app/target

##runner
FROM openjdk:15-alpine
LABEL maintainer="mail@ngeorg.de"

RUN mkdir -p /app
WORKDIR /app

COPY --from=builder /app/target/*.jar /app/*.jar
RUN ls -ll /app
COPY ./startscript.sh /script/start.sh
RUN chmod +x /script/start.sh

# Run the startscript for the jar
ENTRYPOINT ["/script/start.sh", "/app/*.jar"]
