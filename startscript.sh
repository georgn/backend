#!/bin/sh
set -eu
export DISPLAY=:0

if [ ${GUBED} = "enabled" ]; then
    echo "DEBUG MODE ENABLED"

    java -Xdebug -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -Djava.security.egd=file:/dev/./urandom -jar $1
else
    java -Djava.security.egd=file:/dev/./urandom -jar $1
fi