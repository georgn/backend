package de.ngeorg.nanogiant.backend.exception;

public class CompanyAlreadyExistsException extends BadRequestException {
    public CompanyAlreadyExistsException() {
        super("This company or mail already exists");
    }
}
