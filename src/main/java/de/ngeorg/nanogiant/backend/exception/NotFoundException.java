package de.ngeorg.nanogiant.backend.exception;

import java.util.Locale;

public class NotFoundException extends RuntimeException{
    public NotFoundException() {
        super("NOT FOUND");
    }
    public NotFoundException(String object) {
        super(object.toUpperCase(Locale.ROOT) + " NOT FOUND");
    }
}
