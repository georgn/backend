package de.ngeorg.nanogiant.backend.persistence.repository;

import de.ngeorg.nanogiant.backend.persistence.entity.Skill;
import de.ngeorg.nanogiant.backend.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {
    Optional<Skill> findByIdAndCreatedBy(Long id, User user);

    Optional<Skill> findByNameAndCreatedBy(String name, User user);
}
