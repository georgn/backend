package de.ngeorg.nanogiant.backend.persistence.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(nullable = false)
    protected String name;

    @Column
    protected String firstname;

    @Column(nullable = false)
    protected String mail;

    @ManyToMany
    @JoinTable
    protected List<Skill> skills;

    @CreationTimestamp
    protected Date created;

    @UpdateTimestamp
    protected Date updated;

    @ManyToOne
    @JoinColumn
    protected User createdBy;
}
