package de.ngeorg.nanogiant.backend.persistence.repository;

import de.ngeorg.nanogiant.backend.persistence.entity.Employee;
import de.ngeorg.nanogiant.backend.persistence.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

    Optional<Employee> findByIdAndCreatedBy(Long id, User createdBy);

    List<Employee> findByCreatedBy(User createdBy);
}
