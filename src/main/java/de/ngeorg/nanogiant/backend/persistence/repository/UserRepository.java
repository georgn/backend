package de.ngeorg.nanogiant.backend.persistence.repository;

import de.ngeorg.nanogiant.backend.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByMail(String mail);

    Optional<User> findByMailOrCompany(String mail, String company);
}
