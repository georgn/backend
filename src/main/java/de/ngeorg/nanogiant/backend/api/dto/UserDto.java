package de.ngeorg.nanogiant.backend.api.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UserDto {
    @NotNull
    protected String mail;
    @NotNull
    protected String company;
    @NotNull
    protected String password;

}
