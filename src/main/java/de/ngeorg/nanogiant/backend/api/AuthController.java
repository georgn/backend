package de.ngeorg.nanogiant.backend.api;

import de.ngeorg.nanogiant.backend.api.dto.UserDto;
import de.ngeorg.nanogiant.backend.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Creates a new User to login with. Creating a user with an already existing mail or company results in a bad request.")
    @ApiResponse(responseCode = "201", description = "User successfully created!")
    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody UserDto userDto) {
        this.userService.registerUser(userDto.getMail(), userDto.getPassword(), userDto.getCompany());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
