package de.ngeorg.nanogiant.backend.api;

import de.ngeorg.nanogiant.backend.api.dto.EmployeeDto;
import de.ngeorg.nanogiant.backend.api.dto.EmployeeResponseDto;
import de.ngeorg.nanogiant.backend.api.dto.SkillDto;
import de.ngeorg.nanogiant.backend.persistence.entity.Employee;
import de.ngeorg.nanogiant.backend.persistence.entity.Skill;
import de.ngeorg.nanogiant.backend.service.UserEmployeeService;
import de.ngeorg.nanogiant.backend.service.UserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private final UserEmployeeService userEmployeeService;
    private final UserService userService;
    private final ModelMapper mapper;

    public EmployeeController(UserEmployeeService userEmployeeService, UserService userService) {
        this.userEmployeeService = userEmployeeService;
        this.userService = userService;
        this.mapper = new ModelMapper();
    }

    @PostMapping
    public ResponseEntity<EmployeeResponseDto> create(@Validated @RequestBody EmployeeDto employeeDto) {
        Employee employee = this.userEmployeeService.create(this.mapper.map(employeeDto, Employee.class));

        return ResponseEntity.ok(this.mapper.map(employee, EmployeeResponseDto.class));
    }

    @PostMapping("/{id}")
    public ResponseEntity<EmployeeResponseDto> create(@Validated @RequestBody EmployeeDto employeeDto, @PathVariable("id") Long id) {
        Employee employee = this.userEmployeeService.edit(this.mapper.map(employeeDto, Employee.class), id);

        return ResponseEntity.ok(this.mapper.map(employee, EmployeeResponseDto.class));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeResponseDto> get(@PathVariable("id") Long id) {
        Employee employee = this.userEmployeeService.getById(id);

        return ResponseEntity.ok(this.mapper.map(employee, EmployeeResponseDto.class));
    }

    @GetMapping
    public ResponseEntity<List<EmployeeResponseDto>> get() {
        List<Employee> employees = this.userEmployeeService.getAll();

        List<EmployeeResponseDto> employeeResponseDtos = this.mapper.map(employees, new TypeToken<List<EmployeeResponseDto>>() {
        }.getType());

        return ResponseEntity.ok(employeeResponseDtos);
    }

    @GetMapping("/skills")
    public ResponseEntity<List<SkillDto>> getSkills() {
        List<Skill> skills = this.userService.getSkills();

        List<SkillDto> skillDtos = this.mapper.map(skills, new TypeToken<List<SkillDto>>() {
        }.getType());

        return ResponseEntity.ok(skillDtos);
    }
}
