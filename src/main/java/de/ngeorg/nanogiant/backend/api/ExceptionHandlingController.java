package de.ngeorg.nanogiant.backend.api;

import com.auth0.jwt.exceptions.TokenExpiredException;
import de.ngeorg.nanogiant.backend.exception.BadRequestException;
import de.ngeorg.nanogiant.backend.exception.NotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ExceptionHandlingController {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlingController.class);

    @Operation(summary = "The specific object couldn't be found")
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> handleNotFoundException(NotFoundException notFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(notFoundException.getMessage());
    }

    @Operation(summary = "Login Token expired")
    @ExceptionHandler(TokenExpiredException.class)
    public ResponseEntity<?> handleTokenExpiredException(TokenExpiredException tokenExpiredException) {
        String message = tokenExpiredException.getMessage();
        if (message.startsWith("The Token has expired")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("The login has expired!");
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Access denied!");
    }

    @Operation(summary = "Invalid request. See error message for more informations")
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> badRequestException(BadRequestException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
    }

    // OTHER
    @Operation(summary = "Internal Server error. Please report this error!")
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleRuntimeException(RuntimeException exception) {
        logger.error(exception.getMessage(), exception.getCause());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }

}
