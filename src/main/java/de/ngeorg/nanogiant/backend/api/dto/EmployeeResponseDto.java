package de.ngeorg.nanogiant.backend.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeResponseDto extends EmployeeDto{
    protected Long id;
}
