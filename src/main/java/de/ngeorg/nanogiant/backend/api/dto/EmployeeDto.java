package de.ngeorg.nanogiant.backend.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmployeeDto {
    protected String name;
    protected String firstname;
    protected String mail;
    protected List<SkillDto> skills;
}
