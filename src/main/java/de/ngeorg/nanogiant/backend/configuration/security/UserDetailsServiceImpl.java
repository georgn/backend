package de.ngeorg.nanogiant.backend.configuration.security;

import de.ngeorg.nanogiant.backend.persistence.entity.User;
import de.ngeorg.nanogiant.backend.persistence.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository applicationUserRepository;

    public UserDetailsServiceImpl(UserRepository applicationUserRepository) {
        this.applicationUserRepository = applicationUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<User> optionalUser = this.applicationUserRepository.findByMail(username);
        User applicationUser = optionalUser.orElseThrow(() -> new UsernameNotFoundException(username));

        return new org.springframework.security.core.userdetails.User(applicationUser.getMail(), applicationUser.getPassword(), Collections.emptyList());
    }
}
