package de.ngeorg.nanogiant.backend.configuration.security;

public class SecurityConstants {
    private SecurityConstants(){
        throw new IllegalStateException("Constant class");
    }

    public static final String SECRET = "MY_SUPER_SECRET_KEY";
    public static final long EXPIRATION_TIME = 36_000_000; // 10 hours
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/auth/login";
}
