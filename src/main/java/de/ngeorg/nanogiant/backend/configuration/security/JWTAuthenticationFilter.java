package de.ngeorg.nanogiant.backend.configuration.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.ngeorg.nanogiant.backend.persistence.entity.User;
import de.ngeorg.nanogiant.backend.persistence.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private UserRepository readService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, UserRepository readService) {
        this.authenticationManager = authenticationManager;
        this.readService = readService;
        this.setFilterProcessesUrl(SecurityConstants.SIGN_UP_URL);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) {
        UsernamePasswordAuthenticationToken upat;
        try {
            User creds = new ObjectMapper().readValue(req.getInputStream(), User.class);

            upat = new UsernamePasswordAuthenticationToken(
                    creds.getMail(),
                    creds.getPassword(),
                    Collections.emptyList()
            );
        } catch (IOException e) {
            upat = new UsernamePasswordAuthenticationToken(
                    "",
                    "",
                    Collections.emptyList()
            );
        }

        return this.authenticationManager.authenticate(upat);

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) {

        User user = this.readService.findByMail(((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername()).orElse(null);

        if (user != null) {
            String token = JWT.create()
                    .withSubject(user.getMail())
                    .withClaim("userId", user.getId())
                    .withClaim("username", user.getMail())
                    .withExpiresAt(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                    .sign(Algorithm.HMAC512(SecurityConstants.SECRET));
            res.addHeader("Access-Control-Expose-Headers", SecurityConstants.HEADER_STRING);
            res.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.getOutputStream().println("UNAUTHORIZED");

        SecurityContextHolder.clearContext();
    }
}
