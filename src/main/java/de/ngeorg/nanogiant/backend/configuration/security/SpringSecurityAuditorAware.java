package de.ngeorg.nanogiant.backend.configuration.security;

import de.ngeorg.nanogiant.backend.exception.NotFoundException;
import de.ngeorg.nanogiant.backend.persistence.entity.User;
import de.ngeorg.nanogiant.backend.persistence.repository.UserRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Configuration
@EnableJpaAuditing()
public class SpringSecurityAuditorAware implements AuditorAware<User> {

    private final TokenReader tokenReader;
    private final UserRepository userRepository;

    public SpringSecurityAuditorAware(TokenReader tokenReader, UserRepository userRepository) {
        this.tokenReader = tokenReader;
        this.userRepository = userRepository;
    }

    /**
     * Gets the current working user
     *
     * @return Optional of the current user
     */
    @Override
    public Optional<User> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }

        Optional<User> userOptional = this.userRepository.findByMail(this.tokenReader.getMail());
        return userOptional.map(Optional::of).orElse(null);
    }

    /**
     * Gets the current working user
     *
     * @return Optional of the current user
     * @throws NotFoundException if no user is logged in
     */
    public User getAuditor() {
        return this.getCurrentAuditor().orElseThrow(NotFoundException::new);
    }
}
