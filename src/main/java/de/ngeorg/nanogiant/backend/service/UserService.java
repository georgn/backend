package de.ngeorg.nanogiant.backend.service;

import de.ngeorg.nanogiant.backend.configuration.security.SpringSecurityAuditorAware;
import de.ngeorg.nanogiant.backend.exception.CompanyAlreadyExistsException;
import de.ngeorg.nanogiant.backend.persistence.entity.Skill;
import de.ngeorg.nanogiant.backend.persistence.entity.User;
import de.ngeorg.nanogiant.backend.persistence.repository.SkillRepository;
import de.ngeorg.nanogiant.backend.persistence.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    protected final static String[] DEFAULT_SKILLS = {"Vue.js", "React", "Angular", "node.js", "nest.js", "Typescript"};

    protected final UserRepository userRepository;
    protected final SkillRepository skillRepository;
    protected final BCryptPasswordEncoder bCryptPasswordEncoder;
    protected final SpringSecurityAuditorAware auditorAware;

    public UserService(UserRepository userRepository,
                       SkillRepository skillRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder,
                       SpringSecurityAuditorAware auditorAware) {
        this.userRepository = userRepository;
        this.skillRepository = skillRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.auditorAware = auditorAware;
    }

    /**
     * register an user for this application
     *
     * @param mail     users email address. Needs to be unique in the database
     * @param password the password to login
     * @throws CompanyAlreadyExistsException if an user with this email or company already exists
     */
    public void registerUser(String mail, String password, String company) {
        Optional<User> existingUser = this.userRepository.findByMail(mail);

        if (existingUser.isPresent()) {
            throw new CompanyAlreadyExistsException();
        }

        User user = new User();
        user.setMail(mail);
        user.setPassword(this.bCryptPasswordEncoder.encode(password));
        user.setCompany(company);

        // Add default SKills to Database
        List<Skill> skills = new ArrayList<>();
        for (String skill : DEFAULT_SKILLS) {
            Skill skillEntity = new Skill();
            skillEntity.setName(skill);
            skillEntity.setCreatedBy(user);

            skills.add(skillEntity);
        }
        user.setSkills(skills);

        this.userRepository.save(user);
    }

    public List<Skill> getSkills() {
        return this.auditorAware.getAuditor().getSkills();
    }
}
