package de.ngeorg.nanogiant.backend.service;

import de.ngeorg.nanogiant.backend.configuration.security.SpringSecurityAuditorAware;
import de.ngeorg.nanogiant.backend.exception.NotFoundException;
import de.ngeorg.nanogiant.backend.persistence.entity.Employee;
import de.ngeorg.nanogiant.backend.persistence.entity.Skill;
import de.ngeorg.nanogiant.backend.persistence.entity.User;
import de.ngeorg.nanogiant.backend.persistence.repository.EmployeeRepository;
import de.ngeorg.nanogiant.backend.persistence.repository.SkillRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserEmployeeService {

    protected EmployeeRepository employeeRepository;
    protected SkillRepository skillRepository;
    protected SpringSecurityAuditorAware auditorAware;

    public UserEmployeeService(EmployeeRepository employeeRepository, SkillRepository skillRepository, SpringSecurityAuditorAware auditorAware) {
        this.employeeRepository = employeeRepository;
        this.skillRepository = skillRepository;
        this.auditorAware = auditorAware;
    }

    public Employee getById(Long id) {
        User currentUser = this.auditorAware.getAuditor();

        return this.employeeRepository.findByIdAndCreatedBy(id, currentUser).orElseThrow(() -> new NotFoundException(Employee.class.getSimpleName()));
    }

    public List<Employee> getAll() {
        User currentUser = this.auditorAware.getAuditor();

        return this.employeeRepository.findByCreatedBy(currentUser);
    }

    public Employee create(Employee employee) {
        User currentUser = this.auditorAware.getAuditor();

        employee.setId(null);
        employee.setCreatedBy(currentUser);
        employee.setSkills(refreshSkills(employee.getSkills()));

        return this.employeeRepository.save(employee);
    }

    public Employee edit(Employee employee, Long id) {
        Employee oldEmployee = this.getById(id);

        oldEmployee.setFirstname(employee.getFirstname());
        oldEmployee.setName(employee.getName());
        oldEmployee.setMail(employee.getMail());
        oldEmployee.setSkills(refreshSkills(employee.getSkills()));

        return this.employeeRepository.save(oldEmployee);
    }

    /**
     * Reattach the skill entity to the current context
     *
     * @param skills List with skills to reattach
     * @return a List of reattached skills
     */
    private List<Skill> refreshSkills(List<Skill> skills) {
        List<Skill> refreshedSkills = new ArrayList<>();

        User currentUser = this.auditorAware.getAuditor();
        for (Skill tmpSkill : skills) {
            Optional<Skill> skillOptional = this.skillRepository.findByNameAndCreatedBy(tmpSkill.getName(), currentUser);

            Skill skillEntity = skillOptional.orElseThrow(() -> new NotFoundException(Skill.class.getSimpleName() + " " + tmpSkill.getName()));
            refreshedSkills.add(skillEntity);
        }

        return refreshedSkills;
    }

}
